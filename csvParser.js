const Sequelize = require('sequelize');
const moment = require('moment');
const fs = require('fs');
const parse = require('csv-parse');
const async = require('async');
const express = require('express');
const app = express();

const inputFile = 'data.csv';

// 'database', 'username', 'password'
const sequelize = new Sequelize('targem', 'targem', 'targem', {
    host: 'localhost',
    dialect: 'postgres',

    pool: {
        max: 5,
        min: 0,
        idle: 10000
    },

    logging: false
});

const User = sequelize.define('user', {
    username: Sequelize.STRING,
    email: Sequelize.STRING,
    registrationDate: Sequelize.INTEGER,
    status: Sequelize.BOOLEAN,
});


var usersData = [];

async.series([
    function(next) {
        User.sync({force: true}).then(() => {next()});
    },
    function(next) {
        var parser = parse({delimiter: ';'}, function (err, data) {
            var total = data.length;
            var counter = 0;

            console.log('users in file:',total);

            async.eachSeries(data, function (item, callback) {
                counter++;
                printProgress('['+(counter/total*100).toFixed(2)+'%] processing user: '+counter);
                var userData = {
                        username: item[0].trim(),
                        email: item[1].trim(),
                        registrationDate: moment(item[2].trim(), 'DD.MM.YYYY HH:mm').unix(),
                        status: item[3].trim() == 'On' ? true : false,
                    };

                User.build(userData)
                    .save()
                    .then(anotherTask => {
                        callback()
                    })
                    .catch(error => {
                        callback(error)
                    })
            }, next);
        });

        fs.createReadStream(inputFile).pipe(parser);
    },
    function(callback) {
        User.findAll().then(users => {
            console.log("\r\n");
            console.log('csv imported,', users.length, 'users in db');
            app.listen(3000, function () {
                console.log('go check http://localhost:3000');
            });
        });
    }
]);


app.get('/', function (req, res) {
    var query = {
        where: {
            status: true,
        },
        order: [
            ['registrationDate', 'ASC'],
        ],
    }

    User.findAll(query).then(users => {
        // i'm sorry but i wanted to keep it simple
        var response = '<html><head><meta charset=\"UTF-8\"></head><body><table><tr><th>Ник</th><th>Email</th><th>Зарегистрирован</th><th>Статус</th></tr>';

        users.forEach(function(el) {
            el.registrationDate = moment.unix(el.registrationDate).format('DD.MM.YYYY HH:mm');
            el.status = el.status ? 'On' : 'Off';
            response += '<tr><td>'+el.username+'</td><td>'+el.email+'</td><td>'+el.registrationDate+'</td><td>'+el.status+'</td></tr>';
        });
        response += '</table></body></html>';

        res.end(response);
    });
});

function printProgress(text){
    process.stdout.clearLine();
    process.stdout.cursorTo(0);
    process.stdout.write(text);
}
